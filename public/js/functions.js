(function( $ ) {
    $(document).ready(function (){
        $(document).on('click','#access-button',function(event){
            event.preventDefault();
            $(".entry-error").css("display", "none");
            let email = document.getElementsByClassName('entry-email');
            if(email[0].value === '' || 
            email[0].value === email[0].defaultValue ||  
            !email[0].value.includes('@')){
                $(".entry-error").css("display", "flex");
            }
        });
    })
})( jQuery );